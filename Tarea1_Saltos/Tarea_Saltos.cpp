#include <iostream>
#include <fstream>
#include <string>
#include <bitset>
#include <math.h>  

using namespace std; 

/* 
Discripción general: Esta función se encarga de recibir el tipo de predictor a utilizar y crear un archivo con el nombre del predictor escogido. Dentro de este se escribe un encabezado para los posteriores resultados.
Parámetros de entrada: tipo de predicción.
Parametros de salida: se retorna el archivo creado para almacenar resultados.
*/
string create_file(string pred_type){
    
    // Se escoge el nombre del archivo dependiendo del número ingresado a la función.
    string chosen_predictor;
    if (pred_type == "0"){
        chosen_predictor = "Bimodal";
    } else if (pred_type == "1") {
        chosen_predictor = "PShare";
    } else if (pred_type == "2"){
        chosen_predictor = "GShare";
    } else {
        chosen_predictor = "Tournament";
    }

    // Se escribe en el archivo el encabezado para los resultados.
    string pred_type_file = chosen_predictor + ".txt";
    ofstream the_file;
    the_file.open(pred_type_file);
    the_file << "PC           Outcome   Prediction   correct/incorrect\n";
    the_file.close();

    return pred_type_file;
}

/*
Descripción general: Esta función se encarga de escribir en el archivo creado los resultados de cada predicción.
Parámetros de entrada: el archivo a escribir, el PC de la instrucción en forma decimal, el resultado real de si el salto se tomó o no, la predicción hecha por el programa y un string que indica si la predicción fue correcta o no.
Parámetros de salida: no se retorna nada.
*/
void write_to_file(string pred_type_file, long int PC, char outcome, char prediction, string correctness){
    
    // Se abre el archivo con la opción de append, agrega al archivo en vez de sobreescribir. 
    ofstream the_file;
    the_file.open(pred_type_file, ios_base::app);

    // Se escribe en el archivo respetando los espacios por columna.
    the_file << PC << "   " << outcome << "         " << prediction << "            " << correctness << "\n";

    the_file.close();
}   

/*
Descripción general: Esta función de encarga de imprimir los parámetros ingresados para la predicción, además de los resultados de la predicción. El formato es tipo tabla. Además se encarga de calcular el porcentaje de saltos correctamente predichos.
Parámetros de entrada: El parámetro s para poder calcular el tamaño del BHT, el tipo de predictor escogido, el tamaño del registro de historia del GShare, el tamaño de los registros de historia del PShare, el contador de predicciones correctas de saltos tomados, el contador de predicciones incorrectas de saltos tomados, el contador de predicciones correctas de saltos no tomados, el contador de predicciones incorrectas de saltos no tomados, el contador de los saltos totales.
Parámetros de salida: no se retorna nada.
*/
void print_information(int s, string pred_type, int global_pred_size, int private_pred_size, long int CTC, long int ITC, long int CNC, long int INC, long int total_branches){

    // Se calula el número de entradas del BHT.
    int BHT_size = pow(2,s);

    // Se calcula el porcentaje de saltos correctamente predichos. Se hacen casts a double para que los cálculos se efectuen correctamente.
    double porcentage_correct = ((static_cast<double>(CTC) + static_cast<double>(CNC))/static_cast<double>(total_branches))*100;

    // Se imprimi toda la información.
    cout << "---------------------------------------------------------------------" << endl;
    cout << "Prediction parameters:" << endl;
    cout << "---------------------------------------------------------------------" << endl;
    cout << "Branch prediction type:                                " << pred_type << endl;
    cout << "BHT size (entries):                                    " << BHT_size << endl; 
    cout << "Global history register size:                          " << global_pred_size << endl; 
    cout << "Private history register size:                         " << private_pred_size << endl; 
    cout << "---------------------------------------------------------------------" << endl;
    cout << "Simulation results:" << endl;    
    cout << "---------------------------------------------------------------------" << endl;  
    cout << "Total branches:                                        " << total_branches << endl;
    cout << "Number of correct prediction of taken branches:        " <<  CTC << endl; 
    cout << "Number of incorrect prediction of taken branches:      " <<  ITC << endl;
    cout << "Correct prediction of not taken branches:              " <<  CNC << endl;
    cout << "Incorrect prediction of not taken branches:            " <<  INC << endl;
    cout << "Porcentage of correct predictions:                     " << porcentage_correct << "%" << endl;
    cout << "---------------------------------------------------------------------" << endl;
}

/*
Descripción general: Esta función crea un array que representa la estructura de un BHT. Además, se encarga de llenar todas las entradas con ceros, que representan el estado Strongly Not Taken o Strongly Preferred PShare en caso del metapredicor.
Parámetros de entrada: El parámetro s para poder calcular el tamaño del BHT.
Parámetros de salida: Se retorna el array con los estados en cero.
*/
int* create_array(int s){

    // Se calcula el número de entradas que tendrá el BHT (tamaño del array) y se declara el array.
    int size = pow(2,s);
    int* BHT_array = new int[size];

    // Se llena el array con ceros.
    for (int i = 0; i < size; i++)  
    {
        BHT_array[i] = 0;
    }

    return BHT_array;
}

/*
Descripción general: Esta función es la implementación del BHT. Se encarga de tomar el estado del contador solicitado de una de sus entradas y retornarlo, además de actualizar ese mismo contador con el resultado real del salto.
Parámetros de entrada: el índice que indica cuál contador se utilizará y actualizará, el resultado real de si la predicción se tomó o no, el array que representa le estructura del BHT.
Parámetros de salida: el estado (predicción) tomado del contador indicado.
*/
char BHT(int counter_pos, char real_branch, int* BHT_array){

    // Se toma el estado de la indexación hecha y se convierte a su caracter correspondiente. N para Not Taken y T para Taken.
    char prediction;
    if (BHT_array[counter_pos] == 0 || BHT_array[counter_pos] == 1){
        prediction = 'N';
    } else {
        prediction = 'T';
    }

    // Se actualiza el contador de acuerdo al resultado real del salto. Se toman en cuenta los casos extremos para mantener los contadores dentro del rango de estados.
    if (prediction != real_branch){
        if (real_branch == 'T'){
            BHT_array[counter_pos] += 1; 
        }else{
            BHT_array[counter_pos] -= 1;
        }
    }else{ // prediction == real_branch
        if (real_branch == 'T' && BHT_array[counter_pos] != 3){
            BHT_array[counter_pos] += 1;
        }
        if (real_branch == 'N' && BHT_array[counter_pos] != 0){
            BHT_array[counter_pos] -= 1;
        }
    }
    return prediction;
}

/*
Descripción general: Esta función corresponde al predictor Bimodal. Utiliza el PC en su forma binaria para tomar de este los bits necesarios para indexar la tabla BHT. Utiliza la función BHT para actualizar los estados de su tabla BHT.
Parámetros de entrada: El PC en binario, el parámetro s que indica cuántos bits tomar del PC, el resultado real de si el salto se tomó o no, su tabla BHT.
Parámetros de salida: se retorna le predicción hecha por el predictor Bimodal.
*/
char Bimodal_predictor(string PC, int s, char real_branch, int* BHT_array){

    // Se toman los últims s bits del PC en binario y se convierten a decimal con la función stoi, para poder tener el índice de la tabla BHT que almacena el contador a usar y actualizar. 
    string bits_taken_PC = PC.substr(32-s,s);
    int counter_pos = stoi(bits_taken_PC, nullptr, 2);

    // Se llama a la función BHT para extraer la predicción y actulizar el contador que almacena el estado.
    char bimodal_prediction = BHT(counter_pos,real_branch,BHT_array);

    return bimodal_prediction;
}

/*
Descripción general: Esta función representa el predictor GShare. Utiliza el PC en su forma binaria para que junto con el registro de historia global se obtenga el índice correspondiente de la entrada del BHT del cual tomar el el estado (predicción). Además, esta función se encarga de actualizar el registro de historia con el resultado del salto recién efectuado. El registro de historia se pasa como un puntero para poder actualizarlo.
Parámetros de entrada: El PC en binario, el parámetro s que indica cuántos bits tomar del PC, el número de bits que indican el tamaño del registro de historia, el registro de historia, el resultado real de si el salto se tomó o no, su tabla BHT.
Parámetros de salida: se retorna le predicción hecha por el predictor GShare.
*/
char GShare_predictor(string PC, int s, int num_history_bits, string* history_register, char real_branch, int* BHT_array){

    // Se toman los últimos s bits del PC en binario y se accede al puntero que almacena el registro de historia para guardarlo en una variable.
    string bits_taken_PC = PC.substr(32-s,s);
    string history_bits_binary = *history_register;
    
    // Se realiza la operación XOR que permite obtener el índice de la entrada de la tabla BHT que tiene el estado a tomar, se deja que la XOR se complete a 32 bits para permitir el tamaño flexible. Luego se toman los bits necesarios (con el parámetro s) y se convierten a decimal con stoi para obtener el índice.
    auto XOR = bitset<32>(bits_taken_PC) ^ bitset<32>(history_bits_binary);
    string taken_bits_for_BHT = (XOR.to_string()).substr(32-s,s);
    int counter_pos = stoi(taken_bits_for_BHT, nullptr, 2); //convierte el string binario a int decimal
    
    // Se llama a la función BHT para extraer la predicción y actulizar el contador que almacena el estado.
    char GShare_prediction = BHT(counter_pos,real_branch,BHT_array);

    // Se actualiza el registro de historia con el resuladdo real del salto, respetando el tamaño del registro de historia dado. Se realizó con strings para poder tomar ventaja de las funciones de slicing.
    string history_bit;
    if (real_branch == 'T'){
        history_bit = "1";
    } else {
        history_bit = "0";
    }
    *history_register = history_register->substr(1,num_history_bits-1) + history_bit;

    return GShare_prediction;
}

/*
Descripción general: Esta función representa el predictor PShare. Utiliza el PC en su forma binaria para que junto con el registro de historia global se obtenga el índice correspondiente de la entrada del BHT del cual tomar el el estado (predicción). También usa ese mismo PC para indexar la tabla PHT de la cual tomar el registro de historia que se utilizará para esa predicción. Además, esta función se encarga de actualizar el registro de historia con el resultado del salto recién efectuado.
Parámetros de entrada: El PC en binario, el parámetro s que indica cuántos bits tomar del PC, el número de bits que indican el tamaño de los registros de historia, el PHT, el resultado real de si el salto se tomó o no, su tabla BHT.
Parámetros de salida: se retorna le predicción hecha por el predictor PShare.
*/
char PShare_predictor(string PC, int s, int num_history_bits, string* PHT, char real_branch, int* BHT_array){

    // Se toman los últimos s bits del PC en binario y se utilizan para obtener el índice en decimal (con stoi) de la entrada del PHT a utilizar.
    string bits_taken_PC = PC.substr(32-s,s);
    int PHT_pos = stoi(bits_taken_PC, nullptr, 2);

    // Se guarda el registro a utilizar en una variable.
    string chosen_HR = PHT[PHT_pos];

    // Se realiza la operación XOR que permite obtener el índice de la entrada de la tabla BHT que tiene el estado a tomar, se deja que la XOR se complete a 32 bits para permitir el tamaño flexible. Luego se toman los bits necesarios (con el parámetro s) y se convierten a decimal con stoi para obtener el índice.
    auto XOR = bitset<32>(bits_taken_PC) ^ bitset<32>(chosen_HR);
    string taken_bits_for_BHT = (XOR.to_string()).substr(32-s,s);
    int counter_pos = stoi(taken_bits_for_BHT, nullptr, 2);

    // Se llama a la función BHT para extraer la predicción y actulizar el contador que almacena el estado.
    char PShare_prediction = BHT(counter_pos,real_branch,BHT_array);

    // Se actualiza el registro de historia con el resuladdo real del salto, respetando el tamaño del registro de historia dado.
    string history_bit;
    if (real_branch == 'T'){
        history_bit = "1";
    } else {
        history_bit = "0";
    } 
    PHT[PHT_pos] = chosen_HR.substr(1,num_history_bits-1) + history_bit;

    return PShare_prediction;
}

/*
Descripción general: Esta función representa el predictor PShare. Utiliza el PC en su forma binaria para indexar el metapredictor, el cual tiene sus entradas estados para decidir si se utliza la predicción del PShare o el GShare. Además, esta función se encarga de actualizar el metapredictor con el resultado real de si el salto se tomó o no.
Parámetros de entrada: El PC en binario, el parámetro s que indica cuántos bits tomar del PC, el resultado real de si el salto se tomó o no, el número de bits que indican el tamaño de los registros de historia para el PShare y el GShare, el PHT, el registro de historia global, las tablas BHT del PShare y el GSHare, el metapredictor (que tiene la misma estructura que una tabla BHT).
Parámetros de salida: se retorna le predicción hecha por el predictor de Torneo.
*/
char Tournament_predictor(string PC, int s, char real_branch, int num_history_bits_PShare, int num_history_bits_GShare, string* PHT, string* HR_GShare, int* BHT_PShare,int* BHT_GShare, int* metapredictor){

    // // Se toman los últimos s bits del PC en binario y se utilizan para obtener el índice en decimal (con stoi) de la entrada del metapredictor a utilizar.
    char chosen_predictor;
    string bits_taken_PC = PC.substr(32-s,s);
    int counter_pos = stoi(bits_taken_PC, nullptr, 2);
    
    // Se toma el estado de la indexación hecha y se convierte a su caracter correspondiente. P para PShare y G para GShare.
    if (metapredictor[counter_pos] == 0 || metapredictor[counter_pos] == 1){
        chosen_predictor = 'P'; 
    } else {
        chosen_predictor = 'G'; 
    }

    // Se llaman a los dos predictores (PShare y GShare para que hagan sus respectivas predicciones)
    char PShare_prediction = PShare_predictor(PC,s,num_history_bits_PShare,PHT,real_branch,BHT_PShare);
    char GShare_prediction = GShare_predictor(PC,s,num_history_bits_GShare,HR_GShare,real_branch,BHT_GShare);

    // Con el metapreictor se escoge cual predicción es que la que se retorna, además se actualizan el contador respectivo respetando el límite de estados.
    if (chosen_predictor == 'P'){ 
        if (PShare_prediction != GShare_prediction){
            if (PShare_prediction == real_branch && metapredictor[counter_pos] != 0){
                metapredictor[counter_pos] -= 1;  
            }
            if (PShare_prediction != real_branch){
                metapredictor[counter_pos] += 1;
            }
        }
        return PShare_prediction;
    // chosen_predictor == 'G' 
    }else{ 
        if (PShare_prediction != GShare_prediction){
            if (GShare_prediction == real_branch && metapredictor[counter_pos] != 3){
                metapredictor[counter_pos] += 1;  
            }
            if (GShare_prediction != real_branch){
                metapredictor[counter_pos] -= 1;
            }
        }
        return GShare_prediction;
    }
}

/*
Descripción general: Esta función se encarga de la inicalización del registro de historia global con ceros (estados Strongly Not Taken) y tammbién se encarga de la declaración e inicialización de la tabla PHY, con cada registro de historia también con ceros.
Parámetros de entrada: El tipo de predictor a utilizar, el parámetro s que se utliza para calcular el tamaño de la tabla PHT, el tamaño del registro de historia global, el registro de historia global, el tamaño de los registros de historia privada.
Parámetros de salida: No se retorna nada en caso del GShare y se retorna la tabla PHT en caso del PShare.
*/
string* create_history_register(int type_of_pred, int s, int global_pred_size, string* HR_GShare, int private_pred_size){

    // Si el predictor es GShare, se incializa el registro de historia global.
    if (type_of_pred == 2){
        for (int i = 0; i < global_pred_size; i++){
    
        *HR_GShare += "0"; 
        }
    // Si el predictor es PShare, se crea el array para le estructura PHT y se incializan cada una de sus entradas con la cantidad de 0 descrita por el tamaño del registro de historia especificado.
    }else{
        int PHT_size = pow(2,s);
        string* PHT_array = new string[PHT_size];

        for (int i = 0; i < PHT_size; i++){  
            for (int j = 0; j < private_pred_size; j++){
                PHT_array[i] =  PHT_array[i] + "0";
            }
        }
        return PHT_array;
    }
}

int main(int argc, char** argv){

    // Parámetros recibidos desde consola.
    int BHT_size = stoi(argv[2]);
    string pred_type = argv[4];
    int global_pred_size = stoi(argv[6]);
    int private_pred_size = stoi(argv[8]);
    string simul_op = argv[10];

    // Variables para almacenar los distintos valores para el funcionamiento del programa, también para la creación del archivo de salida. Se tienen también variables para los contadores que se encargan de contar los diferentes resultados del programa.
    string trace;
    long int PC; 
    string PC_binary;
    string chosen_predictor;
    string pred_type_file;
    bool write_file = true;
    long int branch_counter = 0;
    long int correct_taken_counter = 0;
    long int incorrect_taken_counter = 0;
    long int correct_nottaken_counter = 0;
    long int incorrect_nottaken_counter = 0;

    // Variables de construcción de estructuras tipo BHT.
    int s = BHT_size;
    int* BHT_Bimodal = create_array(s);
    int* BHT_PShare = create_array(s);
    int* BHT_GShare = create_array(s);
    int* metapredictor = create_array(s);

    // Creación del registro de historia global. Se define un puntero y se llama a la función que se encarga de crear la estructura.
    string HR_GShare = "";  
    string* HR_GShare_pointer = &HR_GShare;
    create_history_register(2,s,global_pred_size, HR_GShare_pointer,private_pred_size);

    // Creación del PHT. Se define una variable y se llama a la función que se encarga de crear la estructura.
    string* PHT = create_history_register(1,s,global_pred_size, HR_GShare_pointer,private_pred_size);

    // Creación del archivo que almacena resultados en caso de ser requerido.
    if (simul_op == "1"){
        pred_type_file = create_file(pred_type);
    }

    // Ciclo para ir salto por salto y realizar su predicción.
    while (getline(cin,trace)){
        
        // Variables que se encargan de extraer el PC y el resultado real de si el salto se tomó o no, de la línea correspondiente. El PC se convierte a binario en este punto también.
        int length_trace = trace.length();
        int PC_length = length_trace - 2;
        PC = stol(trace.substr(0,PC_length));
        PC_binary = bitset<32>(PC).to_string();
        char real_branch = trace[length_trace-1];
        string correctness;

        // Contador de saltos
        branch_counter += 1;

        // Se realiza la predicción de acuerdo al predictor seleccionado por medio de llamar a sus funciones respectivas.
        char prediction;
        if (pred_type == "0"){
            chosen_predictor = "Bimodal";
            prediction = Bimodal_predictor(PC_binary,s,real_branch,BHT_Bimodal);
        } else if (pred_type == "1") {
            chosen_predictor = "PShare";
            prediction = PShare_predictor(PC_binary,s,private_pred_size,PHT,real_branch, BHT_PShare);
        } else if (pred_type == "2"){
            chosen_predictor = "GShare";
            prediction = GShare_predictor(PC_binary,s,global_pred_size,HR_GShare_pointer, real_branch, BHT_GShare);
        } else {
            chosen_predictor = "Tournament";
            prediction = Tournament_predictor(PC_binary,s,real_branch,private_pred_size,global_pred_size,PHT,HR_GShare_pointer,BHT_PShare, BHT_GShare, metapredictor);
        }
        
        // Se actualizan los contadores de resultados y se establece si la predicción fue correcta o no.
        if(prediction == 'T' && real_branch == 'T'){
            correct_taken_counter += 1;
            correctness = "correct";
        }else if(prediction == 'N' && real_branch == 'T'){
            incorrect_taken_counter += 1;
            correctness = "incorrect";
        }else if(prediction == 'N' && real_branch == 'N'){
            correct_nottaken_counter += 1;
            correctness = "correct";
        }else if (prediction == 'T' && real_branch == 'N'){
            incorrect_nottaken_counter += 1;
            correctness = "incorrect";
        }

        // Se escribe en el archivo los resultados obtenidos, si se requirió.
        if (simul_op == "1"){
            if (write_file){
                write_to_file(pred_type_file,PC, real_branch, prediction, correctness);
            }
            if (branch_counter == 5000){
                write_file = false;
            }
        }
        
    }

    // Se imprimie toda la información requerida en consola.
    print_information(s, chosen_predictor, global_pred_size, private_pred_size,correct_taken_counter, incorrect_taken_counter, correct_nottaken_counter, incorrect_nottaken_counter, branch_counter);

    // Se libera la memoria del heap utilizada.
    delete[] BHT_Bimodal;
    delete[] BHT_PShare;
    delete[] BHT_GShare;
    delete[] metapredictor;

    return 0; 
};
