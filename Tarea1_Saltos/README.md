# Marisol Zeledón Córdoba 
# B67969
# Tarea I: Predictores de Saltos 
# I-2020

El programa consta de 4 tipos de predictores de saltos. El usuario es capaz de elegir los parámetros que quiera para los distintos predictores, además de escoger cual de los 4 predictores quiere usar. Se despliegará en consola la información con los parámetros escodigos y los resultados correspondientes. Además, permite la salida de los 5000 primeros resultados a un archivo con el nombre del predictor escogido.
 
Los 4 tipos de predictoes a escoger son el Bimodal, PShare, GShare y Torneo. El predictor Bimodal es simplemente un BHT que se actualiza con cada predicción. Los predictores de PShare y Ghsare requirieron de la construcción de otras estructuras para su funcionamiento. El predictor GShare cuenta con un registro de bits de historia, su tamaño es elegido por el usuario. El predictor PShare cuenta con un PHT de tamaño igual al de su BHT, cada entrada del PHT contiene un registro de bits de historia, cuyo tamaño también es elegido por el usuario. El predictor de Torneo utiliza a su vez, los predictores de PShare y GShare construidos. 

Las indexaciones de las diferentes tablas se usaron utilizando el PC de cada instrucción de salto en su forma binaria, y en el caso del PShare y GShare, por medio de una XOR con el PC y el registro de bits de historia.

# IMPORTANTE #
Los estados están representados por números, como se muestra:
0: Strongly Not Taken o Strongly Preferred PShare para el metapredictor.
1: Weakly Not Taken o Weakly Preferred PShare para el metapredictor. 
2: Weakly Taken o Weakly Preferred GShare para el metapredictor.
3: Strongly Taken o Strongly Preferred GShare para el metapredictor.

Todos los contadores empiezan en el estado 0. 